---
title: TALIB python tuorial 
subtitle: making use of TALIB for financial analysis
date: 2020-01-27
tags: ["python", "talib", 'fintech']
---

```python
import pandas as pd
import talib 
import talib as ta
import numpy as np
from pandas_datareader.data import Options 
from binance.client import Client
from talib import MA_Type

import matplotlib.pyplot as plt

import datetime
from datetime import datetime

%matplotlib inline


#binance

PUBLIC = 'SECRET'
SECRET = 'SECRET'

client = Client(api_key=PUBLIC, api_secret=SECRET)



```


```python
klines = client.get_historical_klines("BTCUSDT", Client.KLINE_INTERVAL_15MINUTE, "15 Jan 2020" ,"18 Jan 2020")

df = pd.DataFrame(klines, columns=["Open time", "Open", "High","Low", "Close", "Volume", "Close time", "Quote asset volume", "Number of trades", "tkb", "tkbq","ignore"])

op,clo,hi,low = df['Open'],df['Close'],df['High'],df['Low']

upper, middle, lower = talib.BBANDS(low, matype=MA_Type.T3, timeperiod=2, nbdevup=2, nbdevdn=2)

data = dict(upper=upper, middle=middle, lower=lower)
result = pd.DataFrame(data, columns=['upper', 'middle', 'lower']).dropna()

#result.plot(figsize=(20,10))
open_time   = [int(entry[0]) for entry in klines]
close       = [float(entry[4]) for entry in klines]
close_array = np.asarray(close)
new_time    = [datetime.fromtimestamp(time/1000) for time in open_time]


macd, macdsignal, macdhist = ta.MACD(close_array, fastperiod=12, slowperiod=26, signalperiod=9)


#plt.style.use('seaborn-white')
plt.style.use('dark_background')

plt.figure(figsize=(20,10))
plt.plot(new_time, macd, label='MACD')
plt.plot(new_time, macdsignal, label='MACD Signal',  color='red')
plt.plot(new_time, macdhist, label='MACD Histogram', color='yellow')
plt.title("MACD Plot for BTC/USDT")
plt.xlabel("Open Time")
plt.ylabel("Value")
plt.legend()
plt.show()




crosses = []
macdabove = False
for i in range(len(macd)):
    if np.isnan(macd[i]) or np.isnan(macdsignal[i]):
        pass
    else:
        if macd[i] > macdsignal[i]:
            if macdabove == False:
                macdabove = True
                cross = [new_time[i],macd[i],'go']
                crosses.append(cross)
        else:
            if macdabove == True:
                macdabove = False
                cross = [new_time[i],macd[i],'ro']
                crosses.append(cross)



plt.style.use('dark_background')
plt.figure(figsize=(20,10))
plt.plot(new_time, macd, label='MACD', color='cyan')
plt.plot(new_time, macdsignal, label='MACD Signal', color='yellow')
for cross in crosses:
    plt.plot(cross[0],cross[1],cross[2])
#plt.plot(new_time, macdhist, label='MACD Histogram')
plt.title("MACD Plot for BTC/USDT")
plt.xlabel("Open Time")
plt.ylabel("Value")
plt.legend()
plt.show()








```


![png](https://gitlab.com/dev-procedural/blog/blob/master/content/post/output_1_0.png)



![png](output_1_1.png)



```python
import mplfinance as mpf
import plotly.graph_objs as go


#quotes = [ list(i) for i in df[['Open', 'High', 'Low', 'Close']].values ]

ty = ['float', 'float', 'float', 'float', 'float', 'int', 'float', 'int', 'str', 'str', 'str'] 
dty = dict(zip(df.axes[1], ty))

#df = df.astype(dty) 

df['Open time'] = df['Open time'].apply( lambda x: datetime.fromtimestamp(x/1000) ) #timestamps to date %Y%M%D

df = df.set_index('Open time')

df.index.name = 'Date'


mpf.plot(df,type='candle')

data=[go.Candlestick(x=df.axes[0],
                open=df['Open'],
                high=df['High'],
                low=df['Low'],
                close=df['Close'])]

go.Figure(data=data)


```


![png](output_2_0.png)


