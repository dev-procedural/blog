---
title: OWASP ZAP PROXY tutorial
subtitle: Owasp zap proxy installation and configuration
date: 2020-01-29
tags: ["ZAP", "Security"]
---


# OWASP ZAP PROXY
## Installation and configuration 

   ZAP is a tool meant for DAST (Dynamic Application Security Testing), this kind of tools help in different kind of process , this time will see how to use the PROXY capability to access all request made to a website. This tutorial will cover the installation and configuration via docker container (Docker installation and config won't be review).

 ### install container
 1.  First things first, let's install it and configure it. We'll make use of the official repository from [OWASP](docker%20run%20-dit%20--name%20zap%20--rm%20-u%20zap%20-p%208080:8080%20-p%208090:8090%20owasp/zap2docker-weekly%20zap-webswing.sh)
 ```bash
 docker run -dit --name zap --rm -u zap -p 8080:8080 -p 8090:8090 owasp/zap2docker-weekly zap-webswing.sh
``` 
 -  Now we can access to localhost:8080/zap
 - at this point we are able to see i our browser the dashboard

### Configuring ZAP
 2. Configure zap to proxy all request to your  browser
 - Let's generate a SSL cert so that browser reach sites, this can be achieve via:
    -    Tools > configure > dynamic ssl cert > save 
    - save the cert in your local env
 - Now le't install the cert in our browser, in this case will be added in brave Chrome/Brave/Chromium.
      - Settings > manage certificates > authorities > import > select the cert location
      
  ### Configuring Browser
  3. Proxy requests from broser to OWASP ZAP with the help of foxy proxy (  [link ](https://chrome.google.com/webstore/detail/foxyproxy-standard/gcknhkkoolaabfmlnjonogaaifnjlfnp?hl=en)  )
  
  4. Adding the new foxy proxy entry
      - foxy proxy > add new  proxy > IP= localhost PORT=8090 
      - select the foxy proxy entry and start catching requests
     

Now we can see the traffic running over the dashboard of the OWASP DASHBOARD